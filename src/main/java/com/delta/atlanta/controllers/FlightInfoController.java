package com.delta.atlanta.controllers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.delta.atlanta.data.Flight;

@RestController
public class FlightInfoController {

	private static List<Flight> flights = new ArrayList<>();
	static {
		flights.add(new Flight(1L, "Flight 1"));
		flights.add(new Flight(2L, "Flight 2"));
		flights.add(new Flight(3L, "Flight 3"));
	}
	
	@GetMapping("/flights")
	public List<Flight> flightsGetAll() {
		return flights;
	}
	
	@GetMapping("/flights/{id}")
	public Flight flightsGet(@PathVariable Long id) {
		Flight flight = getFlightById(id);
		return flight;
	}
	
	@PostMapping("/flights")
	public Flight flightsPost(@RequestBody Flight flight) {
		
		Flight maxIdFlight = flights.stream()
				.max(Comparator.comparing(Flight::getId))
				.get();
		
		long newId = maxIdFlight.getId() + 1;
		flight.setId(newId);
		
		flights.add(flight);
		return flight;
	}
	
	@PutMapping("/flights/{id}")
	public Flight flightsPut(@RequestBody Flight flightNew, @PathVariable Long id) {
		Flight flight = getFlightById(id);
		flight.copyData(flightNew);
		return flight;
	}
	
	@DeleteMapping("/flights/{id}")
	void flightsDelete(@PathVariable Long id) {
		flights.removeIf(flight -> id.equals(flight.getId()));
	} 
	
	//-------------------------------
	
	private Flight getFlightById(Long id) {
		Flight result = flights.stream()
				.filter(flight -> id.equals(flight.getId()))
				.findAny()
				.orElse(null);
		
		return result;
	}
}
