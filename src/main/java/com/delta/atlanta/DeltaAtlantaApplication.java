package com.delta.atlanta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeltaAtlantaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeltaAtlantaApplication.class, args);
	}

}
