package com.delta.atlanta.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Data
public class Flight {
	
	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	private String name;
	
	public Flight(String name) {
		this(null, name);
	}
	
	public void copyData(Flight flightNew) {
		setName(flightNew.getName());
	}
}
